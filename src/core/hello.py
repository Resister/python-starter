from os import environ
import sentry_sdk
from loguru import logger

from src.data.rest.star_wars import planet

sentry_sdk.init(environ["DSN"])


def hello_planet(planet_id: str) -> str:
    logger.info(f"Called planet with id:{planet_id}")
    return f"hello {planet(planet_id).name}!"
