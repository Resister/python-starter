from os import environ
import sentry_sdk
from requests import get

from src.data.rest.model import Planet


def planet(planet_id: str) -> Planet:
    data = get(f" https://swapi.dev/api/planets/{planet_id}").json()
    return Planet(**data)


sentry_sdk.init(environ["DSN"])
