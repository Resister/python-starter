from src.core.hello import hello_planet


def test_returns_hello_tatooine() -> None:
    result = hello_planet("1")
    assert result == "hello Tatooine!"
