# Python Starter &nbsp; <img height="45" style="margin-bottom: -10px" src="https://minio.resistr.life/tmp/python.svg">

![](https://gitlab.com/Resister/python-starter/badges/base/pipeline.svg)
![](https://gitlab.com/Resister/python-starter/badges/base/coverage.svg?job=verify)

## Tooling
### General
- Package Manager: **pipenv**
- Formatting: **black**
- Linting: **pylint**
- Static Checking: **mypy** 
- Testing: **pytest**
- Http Mock: **responses**
- Coverage: **pytest-cov**
- Quality: **codeclimate**
- Commit Formatter: **commitizen**
- Package Security: **safety**
- Logging: **loguru**
- Error Monitoring: **Sentry**


### Schema Gen

-- | In - Pydantic | Out - Dataclass
---|---|---
graphql | [graphql-codegen-pydantic](https://github.com/qw-in/graphql-codegen-pydantic) | [apischema](https://github.com/wyfo/apischema/)
json schema | [datamodel-code-converter](https://github.com/koxudaxi/datamodel-code-generator) |  [apischema](https://github.com/wyfo/apischema)


### CICD (git flows)
![](https://i.imgur.com/B6x9aYa.png)

> see [.gitlab-ci.yml](-/blob/base/.gitlab-ci.yml) for more details

- provider: **gitlab**
- release automation: **semantic release** 
    - plugin:  [@semantic-release/gitlab](https://github.com/semantic-release/gitlab)

### Source Tree
  
```
src
├── core
│     └── __init__.py
├── data
│     ├── graphql
│     │     └── __init__.py
│     ├── __init__.py
│     ├── rest
│     │     └── __init__.py
│     └── sqlachemy
│         └── __init__.py
├── __init__.py
├── ui
│     ├── cli
│     │     └── __init__.py
│     ├── graphql
│     ├── __init__.py
│     └── microservice
│         └── __init__.py
└── utils.py
```